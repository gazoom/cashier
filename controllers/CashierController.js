define([
  '../views/CashierView.js'
], function (CashierView) {

  class CashierController {
    constructor() {
      this.cashierView = new CashierView();
    }

    run() {
    }
  }

  return CashierController;
});
